$(document).ready(function() {


	// Sticky nav
	$('.js--section-features').waypoint(function(direction){
		if (direction == "down") {
			$('nav').addClass("sticky");
		} else {
			$('nav').removeClass("sticky");
		}
	}, {offset: '60px'});


	// Scrolling Effect
	let scrollName = ['features', 'works', 'cities', 'plans'];
	let effectAnimation = ['fadeIn', 'slideInUp', 'fadeIn', 'pulse'];
	let count = 0;


	scrollName.forEach((val, key) => {
		$(`.js--scroll-to-${val}`).on('click', () => {
			$('html, body').animate({
				scrollTop: $(`.js--section-${val}`).offset().top
			}, 1000);
		});

		$(`.js--effect-item-${val}`).waypoint(function(direction) {
			$(`.js--effect-item-${val}`).addClass(`animated ${effectAnimation[key]}`);
		}, {offset: '50%'});
	});

	// Mobile navi
	$('.js--nav-icon').click(() => {
		let nav = $('.js--main-nav');
		let icon = $('.js--nav-icon i');

		nav.slideToggle(200);

		if( icon.hasClass('ion-navicon-round')) {
			icon.addClass('ion-close-round');
			icon.removeClass('ion-navicon-round');
		} else {
			icon.addClass('ion-navicon-round');
			icon.removeClass('ion-close-round');
		}
	});

	// animation on header
	let time = 0;
	for(let i = 1; i <= 3; i++) {
		setTimeout(() => {
			$(`.text-${i}`).addClass('animated slideInUp');
		}, time);

		time += 800;
	}

});